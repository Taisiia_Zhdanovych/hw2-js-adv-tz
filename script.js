// Теоретичне питання
// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію `try...catch`.

// 1.Робота з мережевими запитами. Під час виконання мережевих запитів можуть виникати помилки,
// наприклад, недоступність сервера, проблеми з підключенням до Інтернету, некоректні дані,
// які приходять від сервера і т. д. Використовуючи try...catch, можна перехоплювати ці помилки
// та виконувати необхідні дії для їх обробки.

// 2.Робота з базами даних. Під час роботи з базами даних можуть виникати помилки,
// такі як проблеми з підключенням до бази даних, некоректні запити і т. д. Використовуючи try...catch,
// можна перехоплювати ці помилки та виконувати необхідні дії для їх обробки.

// 3.Робота зі сторонніми бібліотеками. Під час використання сторонніх бібліотек можуть виникати помилки,
// такі як неправильні вхідні дані, помилки в самій бібліотеці і т. д. Використовуючи try...catch,
// можна перехоплювати ці помилки та виконувати необхідні дії для їх обробки.

let div = document.getElementById("root");

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];




function showList(array, proplist) {
  
  let count = 0;
  try {
    if (array.length === 0 || proplist.length === 0) {
      throw new Error("Відсутні дані для перевірки");
    }

    array.map((el) => {
      count++;

      try {
        let initial = "Відсутні такі дані: ";
        let errStr = initial;
        proplist.forEach((prop) => {
          if (!el.hasOwnProperty(prop)) {
            errStr += ` ${prop} `;
          }
        });

        if (initial !== errStr) {
          throw new Error(errStr);
        }

        showListOnPage(el, count);
      } catch (err) {
        console.log(`Книга №${count}`);
        console.log(`${err.name}: ${err.message}`);
      }
    });
  } catch (err) {
    console.log(`${err.name}: ${err.message}`);
  }

}


showList(books, ["author", "name", "price"]);
//showList(books, ["author", "name", "price","year","genre"]);
//showList([], ["author", "name", "price"]);
//showList(books, []);


function showListOnPage(param, num) {
  let ul = document.createElement("ul");
  ul.innerText = `Книга №${num}`;
  div.append(ul);

  for (const [key, value] of Object.entries(param)) {
    ul.insertAdjacentHTML("beforeend", `<li> ${key} - ${value}</li>`);
  }
}